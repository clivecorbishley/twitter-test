"use strict";

var colors = require('colors');

module.exports = {
  usersReadError: function (path) {
    console.error(colors.red(path + ' is an invalid file.'));
    console.error("Correct Usage: user follows user");
    console.error(colors.green("Example: Clive follows Martin, Allan"));
    process.exit(1);
  },
  tweetsReadError: function (path) {
    console.error(colors.red(path + ' is an invalid file.'));
    console.error("Correct Usage: user> tweet");
    console.error(colors.green("Example: clive> This is a valid tweet."));
    process.exit(1);
  },
  userParseError: function (line, lineNumber) {
    console.error(colors.red('Error while parsing user file.'));
    console.error("Error on line: " + lineNumber);
    console.error("Line: " + line);
    console.error("Correct Usage: user follows user");
    console.error(colors.green("Example: Clive follows Martin, Allan"));
    process.exit(1);
  },
  tweetParseError: function (line, lineNumber) {
    console.error(colors.red('Error while parsing tweet file.'));
    console.error("Error on line: " + lineNumber);
    console.error("Line: " + line);
    console.error("Correct Usage: user> tweet");
    console.error(colors.green("Example: clive> This is a valid tweet."));
    process.exit(1);
  },
  tweetLengthError: function (line, lineNumber) {
    console.error(colors.red('Error while parsing tweet file.'));
    console.error("Error on line: " + lineNumber);
    console.error("Line: " + line);
    console.error("Tweet is too long. Maximum 140 characters");
    console.error("Correct Usage: user> tweet");
    console.error(colors.green("Example: clive> This is a valid tweet."));
    process.exit(1);
  },
  generic: function (str) {
    console.error(colors.red("Error: Shutting Down Process"))
    console.error("Message: " + str);
    process.exit(1);
  }
};
