#!/usr/bin/env node
"use strict";

var _ = require('lodash');
var basecamp = require('./index.js');
var fileReader = require('./fileReader.js');

function formatTweets (tweets) {
  var lines = basecamp.tweetsFileData.split(/\r?\n/).filter(Boolean);
  var tweets = lines.map(function (line) {
    var name = line.split(/>/)[0];
    var tweet = line.replace(/^\w+>\s/,'');
    return {
      "name": name,
      "tweet": tweet
    };
  });
  return tweets;
}

function printTweets (users) {
  fileReader.parseTweetsData(basecamp.tweetsFileData);
  var tweets = formatTweets(users, basecamp.tweetsFileData);
  var sortedUsers = _.sortBy(users, "name");
  _.forEach(sortedUsers, function (user) {
    var relevantTweets = [];
    if (!user.follows) { 
      user.follows = []; 
    }
    console.log(user.name);
    user.follows.push(user.name);
    relevantTweets = _.filter(tweets, function (tweet) {
      return _.includes(user.follows, tweet.name);
    });
    _.forEach(relevantTweets, function (tweet) {
      console.log("\t@" + tweet.name + ": " + tweet.tweet);
    });
  });
}

module.exports = {
  printTweets: printTweets
};
