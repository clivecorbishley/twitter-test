#!/usr/bin/env node
"use strict";

var program = require('commander');
var fileReader = require('./fileReader');
var seq = require("promised-io/promise").seq;
var createUsers = require("./createUsers.js");
var editUsers = require("./editUsers.js");
var printer = require("./printer.js");
var fileReader = require("./fileReader.js");
var errors = require("./errors.js");
var usersFile;
var tweetsFile;
var usersFileData;
var tweetsFileData;


exports.initialize = function () {
  program
    .version('1.0.0')
    .option('-u --users-file <users>', 'Users file is required.')
    .option('-t --tweets-file <tweets>', 'Tweets file is required.')
    .parse(process.argv);

  if (!program.usersFile) {
    errors.generic("Missing user file value");
  }

  if (!program.tweetsFile) {
    errors.generic("Missing tweets file value");
  }

  usersFile = process.cwd() + '/' + program.usersFile;
  tweetsFile = process.cwd() + '/' + program.tweetsFile;
  usersFileData = fileReader.readUsersFile(usersFile);
  tweetsFileData = fileReader.readTweetsFile(tweetsFile);

  exports.tweetsFileData = tweetsFileData;
  exports.usersFile = program.usersFile;
  exports.tweetsFile = program.tweetsFile;

  seq([
    fileReader.parseUsersData,
    createUsers.fromFile,
    createUsers.fromFollowers,
    editUsers.removeDuplicates,
    printer.printTweets],
    usersFileData
  );
}
