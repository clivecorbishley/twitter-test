"use strict";

var fs = require('fs');
var _ = require('lodash');
var colors = require('colors');
var basecamp = require("./index.js");
var errors = require('./errors.js');

function readUsersFile (path) {
  try {
    var usersFileData = fs.readFileSync(path, 'ascii');
    return usersFileData;
  } catch (err) {
    errors.usersReadError(path);
  }
}

function readTweetsFile (path) {
  try {
    var tweetsFileData = fs.readFileSync(path, 'ascii');
    return tweetsFileData;
  } catch (err) {
    errors.tweetsReadError(path)
  }
}

function fetchLineNumber (line, lines) {
  var lineNumber;
  try {
    lineNumber = lines.indexOf(line);
    lineNumber++;
  } catch (err) {
    errors.generic('Cannot pinpoint invalid line.')
  }
  return lineNumber;
}

function parseUsersData (data) {
  var lines = data.split(/\r?\n/).filter(Boolean);
  var validRegex = /^\w+\sfollows\s\w+(, \w+)?/;
  _.forEach(lines, function (line) {
    if (!validRegex.test(line)) {
      var lineNumber = fetchLineNumber(line, lines);
      errors.userParseError(line, lineNumber);
    }
  });
  return data;
}

function parseTweetsData (data) {
  var lines = data.split(/\r?\n/).filter(Boolean);
  var validRegex = /^[A-Z][-a-zA-Z]+>\s/;
  _.forEach(lines, function (line) {
    if (!validRegex.test(line)) {
      var lineNumber = fetchLineNumber(line, lines);
      errors.tweetParseError(line, lineNumber);
    }
    var tweet = line.replace(/^\w+>\s/,'');
    if (tweet.length > 140) {
      var lineNumber = fetchLineNumber(line, lines);
      errors.tweetLengthError(line, lineNumber);
    }
  });
  return data;
}

module.exports = {
  readUsersFile: readUsersFile,
  readTweetsFile: readTweetsFile,
  parseUsersData: parseUsersData,
  parseTweetsData: parseTweetsData,
  fetchLineNumber: fetchLineNumber
};