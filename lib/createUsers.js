"use strict";

var _ = require('lodash');
var basecamp = require("./index.js");

function fromFollowers (users) {
  var a = 0;
  _.times(users.length, function () {
    if (users[a].follows) {
      var follows = users[a].follows;
      var b = 0;
      _.times(follows.length, function () {
        var exists = (users.some(function (user) { 
          return _.isMatch(user, { "name" : follows[b] });
        }));
        if (!exists) {
          var user = {
            "name": follows[b]
          };
          users.push(user);
        }
        b++;
      });
    }
    a++;
  });
  return users;
}

function fromFile (data) {
  var lines = data.split(/\r?\n/).filter(Boolean);
  var users = lines.map(function (line) {
    var arr = line.split(/\W/);
    var name = arr[0];
    arr.splice(0, 2);
    var follows = arr.filter(Boolean);
    return {
      "name": name,
      "follows": follows || []
    };
  });
  return users;
}

module.exports = {
  fromFollowers: fromFollowers,
  fromFile: fromFile
};
