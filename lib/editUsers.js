"use strict";

var _ = require('lodash');

function removeDuplicates (users) {
  var i = 0;
  _.times(users.length, function () {
    var a = _.map(_.filter(users, {"name": users[i].name}));
    if (a.length > 1) {
      var j = 1;
      _.times(a.length - 1, function () {
        a[0].follows = _.union(a[0].follows,a[j].follows);
        _.pull(users, a[j]);
        i--;
        j++;
      });
    }
    i++;
  });
  return users;
}

module.exports = {
  removeDuplicates: removeDuplicates
};
