# Twitter Test

## Install

To install dependencies, run `npm install`

To install app itself, run `npm install -g`

## Usage

Run the app using the user file and tweet file flags

`twitter -u <path-to-users-file> -t <path-to-tweets-file>`

### Assumptions

The app is designed to exit upon error. Assuming we don't want the app to keep running if invalid data is input.
The user will still presented with an error message explaining what went wrong.

## Tests

```
mocha tests

# Watch
mocha tests --watch # npm test
```
