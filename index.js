#!/usr/bin/env node
"use strict";

var program = require('commander');
var colors = require('colors');

program
  .version('0.0.1')
  .option('-u --users-file <users>', 'Users file is required.')
  .option('-t --tweets-file <tweets>', 'Tweets file is required.')
  .parse(process.argv);

console.log(program.usersFile)
console.log(program.tweetsFile)