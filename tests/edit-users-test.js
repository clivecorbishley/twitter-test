var assert = require('chai').assert,
    editUsers = require('../lib/editUsers.js');

describe('EditUsers', function () {
  it('removeDuplicates() should remove duplicate users', function () {
    var data = [
      { "name": "Test" },
      { "name": "Test" },
      { "name": "Joe"}
    ];
    var result = editUsers.removeDuplicates(data);
    assert.equal(result.length, 2);
  });

  it ('removeDuplicates() should merge follows for duplicate users', function () {
    var data = [
      { "name": "Test", "follows": ['Joe'] },
      { "name": "Joe", "follows": ['Test'] },
      { "name": "Test", "follows": ['JoeBloggs']}
    ];
    var result = editUsers.removeDuplicates(data);
    assert.deepEqual(result[0].follows, ['Joe', 'JoeBloggs']);
  });
});
