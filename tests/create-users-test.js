var assert = require('chai').assert,
    createUsers = require('../lib/createUsers.js');

describe('CreateUsers', function () {
  it('fromFollowers() should create users as from valid data', function () {
    var data = [
      { "name": "Test", "follows": ["Joe"]}
    ]
    var result = createUsers.fromFollowers(data);
    assert.equal(result.length, 2);
    assert.equal(result[1].name, "Joe");
  });

  it('fromFile() should create users as from valid data', function () {
    var data = "Joe follows Test\nTest follows Joe";
    var result = createUsers.fromFile(data);
    assert.equal(result.length, 2);
    assert.equal(result[0].name, "Joe");
  });
});