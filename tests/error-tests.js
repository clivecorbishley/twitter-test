var assert = require('chai').assert,
    exec = require('child_process').exec;

describe('Errors', function () {

  it('should exit with error if invalid users file path given', function (done) {
    var cmd = 'twitter -u tests/test-files/users -t tests/test-files/validTweets.txt'
    exec(cmd, function (error, stdout, stderr) {
      assert.include(stderr, "tests/test-files/users is an invalid file.")
      assert.equal(error.code, 1);
      done();
    });
  });

  it('should exit with error if invalid tweets file path given', function (done) {
    var cmd = 'twitter -u tests/test-files/validUsers.txt -t tests/test-files/tweets'
    exec(cmd, function (error, stdout, stderr) {
      assert.include(stderr, "tests/test-files/tweets is an invalid file.")
      assert.equal(error.code, 1);
      done();
    });
  });

  it('should exit with error if user file is malformed', function (done) {
    var cmd = 'twitter -u tests/test-files/invalidUsers.txt -t tests/test-files/validTweets.txt'
    exec(cmd, function (error, stdout, stderr) {
      assert.include(stderr, 'Error while parsing user file.')
      assert.equal(error.code, 1);
      done();
    });
  });

  it('should exit with error if tweet file is malformed', function (done) {
    var cmd = 'twitter -u tests/test-files/validUsers.txt -t tests/test-files/invalidTweets.txt'
    exec(cmd, function (error, stdout, stderr) {
      assert.include(stderr, 'Error while parsing tweet file.')
      assert.equal(error.code, 1);
      done();
    });
  });

  it('should exit with error if tweet file longer than 140 characters', function (done) {
    var cmd = 'twitter -u tests/test-files/validUsers.txt -t tests/test-files/longTweet.txt'
    exec(cmd, function (error, stdout, stderr) {
      assert.include(stderr, 'Tweet is too long. Maximum 140 characters')
      assert.equal(error.code, 1);
      done();
    });
  });

});