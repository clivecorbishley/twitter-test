var assert = require('chai').assert,
    exec = require('child_process').exec,
    fs = require('fs');

describe('Printer', function () {

  var savedStdout;

  before(function (done) {
    var cmd = 'twitter -u tests/test-files/validUsers.txt -t tests/test-files/validTweets.txt'
    exec(cmd, function (error, stdout, stderr) {
      savedStdout = stdout;
      done();
    });
  });

  it("printTweets() output the correct details and correct order", function () {
    var output = fs.readFileSync('tests/test-files/expectedOutput.txt', 'ascii');
    assert.deepEqual(output, savedStdout);
  });

});
