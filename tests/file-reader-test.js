var assert = require('chai').assert,
    fileReader = require('../lib/fileReader.js');

describe('FileReader', function () {
  it('readUsersFile() should be able to read valid ascii file', function () {
    fileReader.readUsersFile('./tests/test-files/validUsers.txt');
  });

  it('readTweetsFile() should be able to read valid ascii file', function () {
    fileReader.readTweetsFile('./tests/test-files/validTweets.txt')
  });

  it('fetchLineNumber() should be able to find a valid line number', function () {
    var line = 'test line 2';
    var lines = [
      'test line 1',
      'test line 2',
      'test line 3'
    ];
    var lineNumber = fileReader.fetchLineNumber(line, lines);
    assert.equal(lineNumber, 2);
  });

  it('parseUsersData() should be able to parse valid data', function () {
    var data = "Joe follows Test\nTest follows Joe";
    var response = fileReader.parseUsersData(data);
    assert.equal(data, response);
  }); 

  it('parseTweetsData() should be able to parse valid data', function () {
    var data = "Joe> Test Tweet\nJoe> 2nd Test Tweet"
    var response = fileReader.parseTweetsData(data);
    assert.equal(data, response)
  });

});
